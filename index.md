---
layout: home
title: Présentation
cover: true
menu: true
order: 0
---

Après un début de carrière dans la Business Intelligence (BI) et une réorientation heureuse dans le développement web, je poursuis mon chemin dans le web et les technologies libres.

A la suite de 3 années au Département de Maine-et-Loire, je suis devenu indépendant pour vous proposer mes services.

# Services

Voici des exemples de missions réalisées :

* Développement site e-commerce avec WordPress ([plus d'informations ici](/projets/fille-ou-garcon/))
* Intégration de maquettes graphiques pour utilisation avec un CMS ([par exemple](/projets/danone-lait/))
* Développement de site (par exemple [POUSH](/projets/poush/) ou [l'Agence Entiité](/projets/agence-entiité/))
* Développement d'évolutions et maintenance sur site existant
* Renfort équipe support pour résolutions de problèmes divers
* Migration de site WordPress + sites statiques vers la plateforme Clever Cloud

# Compétences

Développeur backend à l'origine, les besoins de mes employeurs et ma curiosité m'ont amené à élargir mes compétences.

Depuis que je suis indépendant j'ai évolué vers un profil de développeur fullstack en étant amené à faire de l'intégration, du développement frontend, de la configuration et administration de serveur.

## Développement Web

Javascript: ES6, frameworks (Vuejs, React), Node.js, jQuery 

CSS : SASS, Bootstrap, TailwindCSS 

PHP : Typo3, WordPress (Sage, Gutenberg, Woocommerce, Elementor), CakePHP

Git


## Autres compétences

Linux (Administration d'un serveur personnel, à l'aise avec la ligne de commande, scripts shell)

Container et VM : Docker, Docker compose, nomad, vagrant

Réseau : Apache / Nginx / Traefik

Provisionnement : Ansible
