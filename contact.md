---
menu: true
order: 2
---

# Contact

Envoyez-moi un mail à [ronan {at} 3615yeye.info](mailto:ronan@3615yeye.info)

Venez visiter [mon profil Malt](https://www.malt.fr/profile/ronanlepivaingt){:target="_blank"}, plateforme de mise en relation d'entreprises et de travailleurs indépendants
{::comment}
![Contact Malt](/assets/img/contact/malt_logo.svg)
{:/comment}
