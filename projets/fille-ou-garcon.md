---
title: Fille ou Garçon
image: /assets/img/projects/logo-fille-ou-garcon.svg
image_class: logo
description: >
    Site e-commerce de puériculture
categories: [projets-pro]
hide_description: true
header_lien: https://fillegarcon.fr/
header_texte: Lien vers le site (en ligne, mais plus actif)
order: 2
---

Développemet du site e-commerce de puériculture Fille ou Garçon.

![Page d'accueil du site Fille ou Garçon sur plusieurs types d'appareils](/assets/img/projects/mockup-fille-ou-garcon_1.png){:data-size="2327x1140"}

Technologies :
- WordPress et WooCommerce
- Basé sur le thème Sage
- Style avec TailwindCSS
- Constructeur de pages Gutenberg, blocs personnalisés avec Carbon Fields

Particularités du site :
- Intégration des maquettes graphiques
- Synchronisation avec le logiciel de caisse
- Mise en place de systèmes de paiements
- Déploiements automatisés avec Capistrano
- Intégration de Mailjet pour les mails clients
- Recherche textuelle ou vocale avec Algolia

Personnalisations WooCommerce :
- Fonctionnalité liste de naissance :
    - Liste de produits pour une naissance avec fonctionnalité de partage
    - Possibilité d'acheter un produit et/ou de participer à une cagnotte
- Produits avec variations : possibilité de sélectionner une couleur ou un motif à partir de l'image du produit

L'entreprise est malheureusement en liquidation judiciaire et le site n'est plus actif.
