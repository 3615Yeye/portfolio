---
title: Danone Lait
image: /assets/img/projects/logo-danone.png
image_class: logo
description: >
    Intégration refonte du portail Danone Lait pour les agriculteurs
categories: [projets-pro]
hide_description: true
header_lien: https://www.danone-lait.com/espace-non-connecte
header_texte: Lien vers le site
order: 1
---

Intégration des maquettes graphiques et renfort en développement Typo3 pour la refonte du portail des agriculteurs Danone Lait.

![Page d'accueil du site Danone Lait sur plusieurs types d'appareils](/assets/img/projects/mockup-danone-lait.png){:data-size="2308x1123"}

Technologies :
- Bootstrap
- Chartjs
- Gulp

Particularités :
- Données affichées dans des tableaux et/ou graphiques, mises à jour en AJAX en fonction de paramètres comme la date :

![Page analyses avec un tableau et un graphique mis à jour de manière dynamique](/assets/img/projects/danone-lait-page-analyses.png){:data-size="1648x2148"}
