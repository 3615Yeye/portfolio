---
title: Agence Entiité
image: /assets/img/projects/logo-entiite.png
image_class: logo
description: >
    Agence d'architecture d'intérieur
categories: [projets-pro]
hide_description: true
header_lien: https://agence-entiite.com/
header_texte: Lien vers le site
order: 4
---

Développemet du site vitrine de l'agence d'architecture d'intérieur Entiité.

![Page d'accueil du site de l'Agence Entiité sur plusieurs types d'appareils](/assets/img/projects/mockup-agence-entiite.png){:data-size="2308x1114"}

Technologies :
- WordPress
- Constructeur de pages Gutenberg

Particularités :
- Construit à l'origine sur un thème payant, le site a été refait en conservant le style et sur la base du thème underscores pour améliorer grandement les performances
- Développement d'un bloc personnalisé Gutenberg pour gérer en drag'n'drop la disposition des photos de la page d'accueil et des pages projets
