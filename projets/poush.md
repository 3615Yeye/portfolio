---
title: POUSH
image: /assets/img/projects/logo-poush.png
image_class: logo
description: >
    Résidence d'artistes de la région parisienne
categories: [projets-pro]
hide_description: true
header_lien: https://poush.fr/
header_texte: Lien vers le site
order: 3
---

Développement du site vitrine pour l'incubateur d'artistes POUSH en région parisienne. 

![Page d'accueil du site POUSH sur plusieurs types d'appareils](/assets/img/projects/mockup-poush.png){:data-size="2310x1114"}

Technologies :
- WordPress
- Constructeur de pages Elementor Pro

Particularités :
- Mise en place lecteur pour Radio POUSH avec support du format HLS pour un streaming sur les smartphones iOS
- Fonctionnement bilingue français et anglais
