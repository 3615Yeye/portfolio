---
title: Projets
description: >
  Here you should be able to find everything you need to know to accomplish the most common tasks when blogging with Hydejack.
hide_description: true
menu: true
order: 1
permalink: /projets/
---

{% assign sorted_pages = site.pages | sort:"order" %}

<h2>Personnels</h2>

<div class="cards-container">
  {% for page in sorted_pages %}
    {% if page.categories contains 'projets' %}
      {%include components/card.html page=page %}
    {% endif %}
  {% endfor %}
</div>

<h2>Professionnels</h2>
<p>Voici quelques exemples de projets sur lesquels j'ai pu travailler. </p>
<div class="cards-container">
  {% for page in sorted_pages %}
    {% if page.categories contains 'projets-pro' %}
      {%include components/card.html page=page %}
    {% endif %}
  {% endfor %}
</div>

{:.related-posts.faded}

[Hippy Mood]: hippymood.md
[Minitel]: minitel.md
