# Build
# This build created a staging docker image
#
FROM jekyll/jekyll:3 AS build
RUN mkdir /app && chown jekyll:jekyll /app
WORKDIR /app
COPY Gemfile Gemfile.lock ./
COPY package.json package-lock.json ./
RUN npm install
RUN bundle install
COPY . .
RUN npm run build
RUN JEKYLL_ENV=production bundle exec jekyll build

# Serve the data
# This build takes the production build from staging build
#
FROM httpd:2.4-alpine
COPY --from=build /app/_site/ /usr/local/apache2/htdocs/
EXPOSE 80
